;;; PACKAGE

(require 'package)
(add-to-list 'package-archives
	     '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)
(package-refresh-contents)
;; Define helper to install packages
(defun install-if-not-installed (package)
  (unless (package-installed-p package)
    (package-install package)))

;;; SANITY

(install-if-not-installed 'better-defaults)
;; Disable startup message
(setq inhibit-startup-message t)
;; Answer y/n instead of yes/no
(fset 'yes-or-no-p 'y-or-n-p)
;; Store backups under .emacs.d/
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
;; Remove trailing whitespace on save
(add-hook 'before-save-hook 'delete-trailing-whitespace)
;; Insert closing brackets
(electric-pair-mode)

;;; VISUALS

;; Set font if running in a window
(if window-system
    (set-face-attribute 'default nil :font "Consolas-15"))
;; Install and load Monokai
(install-if-not-installed 'molokai-theme)
(load-theme 'molokai t)
;; Display relative line numbers with a space after
(install-if-not-installed 'linum-relative)
(require 'linum-relative)
(setq-default linum-relative-format "%3s ")
(linum-relative-global-mode)
;; Set tab width
(setq-default tab-width 4)

;;; GIT

(install-if-not-installed 'magit)
(global-set-key (kbd "C-c i") 'magit-status)
;; Windows-specific setup
(when (eq system-type 'windows-nt)
  (install-if-not-installed 'ssh-agency)
  (require 'ssh-agency)
  ;; Add Bitbucket key to ssh-agent
  (add-to-list 'ssh-agency-keys
               "~/.ssh/bitbucket_rsa"))

;;; GO

(install-if-not-installed 'go-mode)
(install-if-not-installed 'go-autocomplete)
(require 'go-autocomplete)
;; Run goimports instead of gofmt
;; Requires `diff` in PATH
(setq gofmt-command "goimports")
(add-hook 'before-save-hook 'gofmt-before-save)

;;; YAML

(install-if-not-installed 'yaml-mode)

;;; AUTOCOMPLETE

(install-if-not-installed 'auto-complete)
;; Configure auto-complete after setting up sources
(require 'auto-complete-config)
(ac-config-default)
